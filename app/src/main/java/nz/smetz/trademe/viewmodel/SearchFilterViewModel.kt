package nz.smetz.trademe.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import nz.smetz.trademe.model.data.SearchQuery
import javax.inject.Inject

/**
 * View model for the search filter fragment
 * Stores the search query to be executed
 *
 * Created by smetz on 10/14/2017.
 */
class SearchFilterViewModel @Inject constructor() : ViewModel() {
    /**
     * Live data containing the current search query
     */
    val searchQuery: MutableLiveData<SearchQuery> = MutableLiveData()

    /**
     * Sets the number of the category to search for listings
     */
    fun setCategory(categoryNumber: String) {
        var query = searchQuery.value
        if (query?.category != categoryNumber) {
            if (query == null) {
                query = SearchQuery(category = categoryNumber)
            } else {
                query.category = categoryNumber
            }
            searchQuery.value = query
        }
    }

    /**
     * Sets the query to use to search for listings
     */
    fun setQuery(query: SearchQuery) {
        if (searchQuery.value != query) {
            searchQuery.value = query
        }
    }
}