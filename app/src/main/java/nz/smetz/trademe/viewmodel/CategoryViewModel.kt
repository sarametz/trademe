package nz.smetz.trademe.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import nz.smetz.trademe.model.data.Category
import nz.smetz.trademe.model.TradeMeService
import nz.smetz.trademe.model.util.ApiResponse
import javax.inject.Inject

/**
 * View model for the category fragment
 *
 * Created by smetz on 10/10/2017.
 */
class CategoryViewModel @Inject constructor(tradeMeService: TradeMeService): ViewModel() {
    /**
     * Live data that stores the number of the category to show
     */
    private val categoryNumber: MutableLiveData<String> = MutableLiveData()

    /**
     * Live data containing the category object to display
     */
    val category: LiveData<ApiResponse<Category>>

    init {
        //Update the category information whenever the categoryNumber changes
        category = Transformations.switchMap(categoryNumber, { number ->
            tradeMeService.getCategory(number)
        })
    }

    /**
     * Sets the number of the category to show
     */
    fun setCategoryNumber(number: String) {
        if (number != categoryNumber.value) {
            categoryNumber.value = number
        }
    }

    /**
     * Refreshes the category data
     */
    fun refresh(){
        if (categoryNumber.value != null) {
            categoryNumber.value = categoryNumber.value
        }
    }
}