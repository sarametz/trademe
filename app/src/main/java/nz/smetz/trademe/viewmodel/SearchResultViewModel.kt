package nz.smetz.trademe.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import nz.smetz.trademe.model.data.SearchResult
import nz.smetz.trademe.model.TradeMeService
import nz.smetz.trademe.model.data.Category
import nz.smetz.trademe.model.data.SearchQuery
import nz.smetz.trademe.model.util.ApiResponse
import javax.inject.Inject

/**
 * View model for the search result fragment
 *
 * Created by smetz on 10/12/2017.
 */
class SearchResultViewModel @Inject constructor(private val tradeMeService: TradeMeService) : ViewModel() {
    /**
     * Live data that stores the number of the category to show
     */
    private val searchQuery: MutableLiveData<SearchQuery> = MutableLiveData()

    /**
     * Live data containing the search results to display
     */
    val searchResult: LiveData<ApiResponse<SearchResult>>

    /**
     * Live data containing the category object to display
     */
    val searchCategory: LiveData<ApiResponse<Category>>

    init {
        //Update the search result information whenever the query changes
        searchResult = Transformations.switchMap(searchQuery, { query ->
            tradeMeService.searchListings(query.toMap())
        })
        //Update the category information whenever the query changes, so that the title can be displayed
        searchCategory = Transformations.switchMap(searchQuery, { query ->
            tradeMeService.getCategory(query.category ?: Category.ROOT_CATEGORY_NUMBER, 0)
        })
    }

    /**
     * Sets the query to use to search for listings
     */
    fun setSearchQuery(newQuery: SearchQuery) {
        if (searchQuery.value != newQuery) {
            searchQuery.value = newQuery
        }
    }

    /**
     * Refreshes the search result data
     */
    fun refresh() {
        if (searchQuery.value != null) {
            searchQuery.value = searchQuery.value
        }
    }
}