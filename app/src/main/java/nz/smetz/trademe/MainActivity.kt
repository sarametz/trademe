package nz.smetz.trademe

import android.app.SearchManager
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_main.toolbar
import kotlinx.android.synthetic.main.header_filter.filter_header
import nz.smetz.trademe.model.data.Category
import nz.smetz.trademe.model.data.SearchQuery
import nz.smetz.trademe.view.NavigationController
import nz.smetz.trademe.view.TradeMeHeaderActivity
import javax.inject.Inject

/**
 * The main activity for the TradeMe app
 */
class MainActivity : AppCompatActivity(), HasSupportFragmentInjector, TradeMeHeaderActivity {
    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var navigationController: NavigationController

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = dispatchingAndroidInjector

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val searchQuery = if (Intent.ACTION_SEARCH == intent?.action)
            intent.getStringExtra(SearchManager.QUERY)
        else
            ""

        if (savedInstanceState == null) {
            val query = SearchQuery(category = Category.ROOT_CATEGORY_NUMBER, search_string = searchQuery)
            if (resources.getBoolean(R.bool.is_tablet_mode)) {
                navigationController.showSearchFilterFragment(query)
            }
            navigationController.navigateToSearch(query)
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)

        val searchQuery = if (Intent.ACTION_SEARCH == intent?.action)
            intent.getStringExtra(SearchManager.QUERY)
        else
            ""

        val query = SearchQuery(category = Category.ROOT_CATEGORY_NUMBER, search_string = searchQuery)
        if (resources.getBoolean(R.bool.is_tablet_mode)) {
            navigationController.showSearchFilterFragment(query)
        }
        navigationController.navigateToSearch(query)

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.options_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.menu_search) {
            onSearchRequested()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        finish()
    }

    override fun getFilterHeader(): View? = filter_header
}
