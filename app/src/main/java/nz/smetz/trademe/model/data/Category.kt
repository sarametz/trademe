package nz.smetz.trademe.model.data

import android.content.Context
import com.google.gson.annotations.SerializedName
import nz.smetz.trademe.R

/**
 * Data object to store category information
 *
 * Created by smetz on 10/9/2017.
 */
data class Category(@SerializedName("Name")
                    val name: String,
                    @SerializedName("Number")
                    val number: String,
                    @SerializedName("Path")
                    val path: String = "",
                    @SerializedName("Subcategories")
                    val subcategories: List<Category>? = null,
                    @SerializedName("Count")
                    val count: Int = 0,
                    @SerializedName("IsRestricted")
                    val isRestricted: Boolean = false,
                    @SerializedName("HasLegalNotice")
                    val hasLegalNotice: Boolean = false,
                    @SerializedName("HasClassifieds")
                    val hasClassifieds: Boolean = false,
                    @SerializedName("AreaOfBusiness")
                    val areaOfBusiness: Int = 0,
                    @SerializedName("CanHaveSecondCategory")
                    val canHaveSecondCategory: Boolean = false,
                    @SerializedName("CanBeSecondCategory")
                    val canBeSecondCategory: Boolean = false) {
    /**
     * Checks if this category is the root category
     * Which is if the number is zero or more zero characters which can optionally be followed by a dash
     */
    fun isRoot(): Boolean = Regex("^0*\\-?\$").containsMatchIn(number)

    val displayPath: String
        get() = path.trim('/').replace("/", " > ").replace("-", " ")

    companion object {
        val ROOT_CATEGORY_NUMBER = "0"

        /**
         * Creates a category entity corresponding to the root category
         *
         * @param context the current context, required to retrieve the category name string
         * @return the root category
         */
        fun root(context: Context): Category {
            return Category(context.getString(R.string.root_category_display_text), ROOT_CATEGORY_NUMBER)
        }
    }
}