package nz.smetz.trademe.model

import android.arch.lifecycle.LiveData
import nz.smetz.trademe.model.data.Category
import nz.smetz.trademe.model.data.SearchResult
import nz.smetz.trademe.model.util.ApiResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.QueryMap

/**
 * TradeMe API Service
 *
 * Created by smetz on 10/9/2017.
 */
interface TradeMeService {
    @GET("Categories.json")
    fun getAllCategories(): LiveData<ApiResponse<Category>>

    @GET("Categories/{number}.json")
    fun getCategory(@Path("number") number: String,
                    @Query("depth") depth: Int = 1,
                    @Query("with_counts") withCounts: Boolean = false,
                    @Query("region") region: Int? = null): LiveData<ApiResponse<Category>>

    @GET("Search/General.json")
    fun searchListings(@QueryMap searchQuery: Map<String, String>): LiveData<ApiResponse<SearchResult>>
}