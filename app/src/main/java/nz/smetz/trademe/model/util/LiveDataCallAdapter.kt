package nz.smetz.trademe.model.util

import android.arch.lifecycle.LiveData
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Callback
import retrofit2.Response
import java.lang.reflect.Type
import java.util.concurrent.atomic.AtomicBoolean

/**
 * Retrofit call adapter to convert a call into LiveData
 * Based on the LiveDataCallAdapter in https://github.com/googlesamples/android-architecture-components/tree/master/GithubBrowserSample
 *
 * Created by smetz on 10/9/2017.
 */
class LiveDataCallAdapter<R>(private val responseType: Type) : CallAdapter<R, LiveData<ApiResponse<R>>> {
    override fun responseType(): Type = responseType

    override fun adapt(retrofitCall: Call<R>?): LiveData<ApiResponse<R>> {
        return object : LiveData<ApiResponse<R>>() {
            internal var started = AtomicBoolean(false)
            override fun onActive() {
                super.onActive()
                if (started.compareAndSet(false, true)) {
                    retrofitCall?.enqueue(object : Callback<R> {
                        override fun onFailure(call: Call<R>?, t: Throwable?) {
                            postValue(ApiResponse<R>(t))
                        }

                        override fun onResponse(call: Call<R>?, response: Response<R>?) {
                            postValue(ApiResponse(response))
                        }

                    })
                }
            }
        }
    }
}