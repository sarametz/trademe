package nz.smetz.trademe.model.util

import com.google.common.net.UrlEscapers
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import okio.Buffer
import okio.ByteString
import java.security.InvalidKeyException
import java.security.NoSuchAlgorithmException
import java.security.SecureRandom
import java.util.TreeMap
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec

/**
 * Okhttp Interceptor to add an OAuth 1 authorization header
 * Based on OAuth Interceptor at https://gist.github.com/JakeWharton/f26f19732f0c5907e1ab
 * TODO: Do proper auth
 *
 * Created by smetz on 10/12/2017.
 */
class OAuth1SigningInterceptor(
        private val consumerKey: String = "A1AC63F0332A131A78FAC304D007E7D1",
        private val consumerSecret: String = "EC7F18B17A062962C6930A8AE88B16C7",
        private val accessToken: String = "706B839898FD547DCFFE25D836DCCA54",
        private val accessSecret: String = "D26DB70A18F7E0CC6238B188F3A3DAF1",
        private val random: SecureRandom = SecureRandom(),
        private val clock: Clock = Clock()
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        return chain.proceed(signRequest(chain.request()))
    }

    /**
     * Adds an OAuth 1.0 authorization header to the given request
     *
     * @param request the request to add the header to
     * @return the request with the authorization header added
     */
    fun signRequest(request: Request): Request {
        val nonce = ByteArray(32)
        random.nextBytes(nonce)
        val oauthNonce = ByteString.of(nonce, 0, 32).base64().replace(Regex("\\W"), "")
        val oauthTimestamp = clock.millis()

        val consumerKeyValue = escaper.escape(consumerKey)
        val accessTokenValue = escaper.escape(accessToken)

        val parameters = TreeMap<String, String>()
        parameters.put(OAUTH_CONSUMER_KEY, consumerKeyValue)
        parameters.put(OAUTH_ACCESS_TOKEN, accessTokenValue)
        parameters.put(OAUTH_NONCE, oauthNonce)
        parameters.put(OAUTH_TIMESTAMP, oauthTimestamp)
        parameters.put(OAUTH_SIGNATURE_METHOD, OAUTH_SIGNATURE_METHOD_VALUE)
        parameters.put(OAUTH_VERSION, OAUTH_VERSION_VALUE)

        val url = request.url()
        for (i in 0..url.querySize() - 1) {
            parameters.put(escaper.escape(url.queryParameterName(i)),
                           escaper.escape(url.queryParameterValue(i)))
        }

        val body = Buffer()

        val requestBody = request.body()
        requestBody?.writeTo(body)

        while (!body.exhausted()) {
            val keyEnd = body.indexOf('='.toByte())
            if (keyEnd == -1L) throw IllegalStateException("Key with no value: " + body.readUtf8())
            val key = body.readUtf8(keyEnd)
            body.skip(1) // Equals.

            val valueEnd = body.indexOf('&'.toByte())
            val value = if (valueEnd == -1L) body.readUtf8() else body.readUtf8(valueEnd)
            if (valueEnd != -1L) body.skip(1) // Ampersand.

            parameters.put(key, value)
        }

        val base = Buffer()
        val method = request.method()
        base.writeUtf8(method)
        base.writeByte('&'.toInt())
        base.writeUtf8(
                escaper.escape(request.url().newBuilder().query(null).build().toString()))
        base.writeByte('&'.toInt())

        var first = true
        for ((key, value) in parameters.toSortedMap()) {
            if (!first) base.writeUtf8(escaper.escape("&"))
            first = false
            base.writeUtf8(escaper.escape(key))
            base.writeUtf8(escaper.escape("="))
            base.writeUtf8(escaper.escape(value))
        }

        val signingKey = "${escaper.escape(consumerSecret)}&${escaper.escape(accessSecret)}"

        val keySpec = SecretKeySpec(signingKey.toByteArray(), "HmacSHA1")
        val mac: Mac
        try {
            mac = Mac.getInstance("HmacSHA1")
            mac.init(keySpec)
        } catch (e: NoSuchAlgorithmException) {
            throw IllegalStateException(e)
        } catch (e: InvalidKeyException) {
            throw IllegalStateException(e)
        }

        val result = mac.doFinal(base.readByteArray())
        val signature = ByteString.of(result, 0, result.size).base64()

        val authorization = "OAuth $OAUTH_CONSUMER_KEY=\"$consumerKeyValue\", $OAUTH_NONCE=\"$oauthNonce\", $OAUTH_SIGNATURE=\"${escaper.escape(
                signature)}\", $OAUTH_SIGNATURE_METHOD=\"$OAUTH_SIGNATURE_METHOD_VALUE\", $OAUTH_TIMESTAMP=\"$oauthTimestamp\", $OAUTH_ACCESS_TOKEN=\"$accessTokenValue\", $OAUTH_VERSION=\"$OAUTH_VERSION_VALUE\""

        return request.newBuilder().addHeader("Authorization", authorization).build()
    }

    class Clock {
        /**
         * Returns the current time in seconds
         */
        fun millis(): String {
            return (System.currentTimeMillis() / 1000L).toString()
        }
    }

    companion object {
        private val OAUTH_CONSUMER_KEY = "oauth_consumer_key"
        private val OAUTH_NONCE = "oauth_nonce"
        private val OAUTH_SIGNATURE = "oauth_signature"
        private val OAUTH_SIGNATURE_METHOD = "oauth_signature_method"
        private val OAUTH_SIGNATURE_METHOD_VALUE = "HMAC-SHA1"
        private val OAUTH_TIMESTAMP = "oauth_timestamp"
        private val OAUTH_ACCESS_TOKEN = "oauth_token"
        private val OAUTH_VERSION = "oauth_version"
        private val OAUTH_VERSION_VALUE = "1.0"
        private val escaper = UrlEscapers.urlFormParameterEscaper()
    }
}