package nz.smetz.trademe.model.util

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type
import java.util.Date

/**
 * Helper class to deserialize dates from Trade Me API responses
 *
 * Created by smetz on 10/12/2017.
 */
class TradeMeDateConverter : JsonDeserializer<Date> {
    override fun deserialize(json: JsonElement?,
                             typeOfT: Type?,
                             context: JsonDeserializationContext?): Date {
        val dateString = json?.asJsonPrimitive?.asString ?: ""
        val dateLong = dateString.substring(dateString.indexOf("(")+1, dateString.indexOf(")")).toLong()
        return Date(dateLong)
    }
}