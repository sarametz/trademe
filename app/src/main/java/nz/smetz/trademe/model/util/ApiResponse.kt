package nz.smetz.trademe.model.util

import retrofit2.Response

/**
 * Class that wraps an API response to store both data and response status
 *
 * Created by smetz on 10/9/2017.
 */
class ApiResponse<T> {
    constructor(error: Throwable?) {
        exception = error
        status = Status.ERROR
    }

    constructor(response: Response<T>?) {
        if (response?.isSuccessful ?: false) {
            data = response?.body()
            status = Status.SUCCESS
        } else {
            exception = Throwable(response?.errorBody()?.string() ?: "Unknown error")
            status = Status.ERROR
        }
    }

    var status: Status = Status.ERROR
    var exception: Throwable? = null
    var data: T? = null

    enum class Status{
        SUCCESS,
        ERROR
    }
}