package nz.smetz.trademe.model.data

import com.google.gson.annotations.SerializedName
import java.util.Date

/**
 * Data object to store listing information
 * TODO: add all properties
 *
 * Created by smetz on 10/11/2017.
 */
data class Listing(@SerializedName("ListingId")
                   val listingId: Int,
                   @SerializedName("Title")
                   val title: String,
                   @SerializedName("Category")
                   val categoryName: String = "",
                   @SerializedName("StartPrice")
                   val startPrice: Double = 0.0,
                   @SerializedName("BuyNowPrice")
                   val buyNowPrice: Double = 0.0,
                   @SerializedName("StartDate")
                   val startDate: Date? = null,
                   @SerializedName("EndDate")
                   val endDate: Date? = null,
                   @SerializedName("IsFeatured")
                   val isFeatured: Boolean = false,
                   @SerializedName("HasGallery")
                   val hasGallery: Boolean = false,
                   @SerializedName("IsBold")
                   val isBold: Boolean = false,
                   @SerializedName("IsHighlighted")
                   val isHighlighted: Boolean = false,
                   @SerializedName("HasHomePageFeature")
                   val hasHomePageFeature: Boolean = false,
                   @SerializedName("MaxBidAmount")
                   val maxBidAmount: Double = 0.0,
                   @SerializedName("CategoryPath")
                   val categoryPath: String = "",
                   @SerializedName("PictureHref")
                   val pictureHref: String = "",
                   @SerializedName("PriceDisplay")
                   val priceDisplay: String = "",
                   @SerializedName("Region")
                   val region: String = "",
                   @SerializedName("IsReserveMet")
                   val isReserveMet: Boolean = false,
                   @SerializedName("HasReserve")
                   val hasReserve: Boolean = false,
                   @SerializedName("HasBuyNow")
                   val hasBuyNow: Boolean = false,
                   @SerializedName("IsBuyNowOnly")
                   val isBuyNowOnly: Boolean = false,
                   @SerializedName("IsClassified")
                   val isClassified: Boolean = false,
                   @SerializedName("ReserveState")
                   val reserveState: Int = 4) {

    enum class ReserveState(val intValue: Int) {
        None(0),
        Met(1),
        NotMet(2),
        NotApplicable(3)
    }
}