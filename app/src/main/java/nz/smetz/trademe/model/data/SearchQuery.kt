package nz.smetz.trademe.model.data

import android.os.Parcel
import android.os.Parcelable
import kotlin.reflect.full.memberProperties

/**
 * Data class to store search query parameters
 *
 * Created by smetz on 10/14/2017.
 */
data class SearchQuery(var buy: Buy? = null,
                       var category: String? = null,
                       var clearance: Clearance? = null,
                       var condition: Condition? = null,
                       var search_string: String? = null) : Parcelable {

    /**
     * Converts the query into a map of property names to values
     */
    fun toMap(): Map<String, String> {
        val map = HashMap<String, String>()
        for (prop in SearchQuery::class.memberProperties) {
            map.put(prop.name, prop.get(this).toString())
        }
        return map
    }

    constructor(parcelIn: Parcel) : this(
            Buy.valueOf(parcelIn.readString()),
            parcelIn.readString(),
            Clearance.valueOf(parcelIn.readString()),
            Condition.valueOf(parcelIn.readString()),
            parcelIn.readString()
    )

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(buy?.toString())
        dest.writeString(category)
        dest.writeString(clearance?.toString())
        dest.writeString(condition?.toString())
        dest.writeString(search_string)
    }

    override fun describeContents() = 0

    enum class Buy {
        All,
        BuyNow
    }

    enum class Clearance {
        All,
        Clearance,
        OnSale
    }

    enum class Condition {
        All,
        New,
        Used
    }

    companion object {
        @JvmField @Suppress("unused")
        val CREATOR = object : Parcelable.Creator<SearchQuery> {
            override fun createFromParcel(source: Parcel): SearchQuery {
                return SearchQuery(source)
            }

            override fun newArray(size: Int): Array<SearchQuery?> {
                return arrayOfNulls(size)
            }
        }
    }
}


