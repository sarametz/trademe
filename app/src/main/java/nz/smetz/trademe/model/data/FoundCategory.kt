package nz.smetz.trademe.model.data

import com.google.gson.annotations.SerializedName

/**
 * Data class to store found category API results
 *
 * Created by smetz on 10/13/2017.
 */
data class FoundCategory(@SerializedName("Count")
                         val count: Int,
                         @SerializedName("Category")
                         val category: String,
                         @SerializedName("Name")
                         val name: String,
                         @SerializedName("IsRestricted")
                         val isRestricted: Boolean,
                         @SerializedName("CategoryId")
                         val categoryId: Int)