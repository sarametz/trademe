package nz.smetz.trademe.model.data

import com.google.gson.annotations.SerializedName

/**
 * Data class for storing a search api result
 * TODO: add all properties
 *
 * Created by smetz on 10/11/2017.
 */
data class SearchResult(@SerializedName("TotalCount")
                        val totalCount: Int = 0,
                        @SerializedName("Page")
                        val page: Int = 0,
                        @SerializedName("PageSize")
                        val pageSize: Int = 0,
                        @SerializedName("List")
                        val list: List<Listing>? = null,
                        @SerializedName("DidYouMean")
                        val didYouMean: String = "",
                        @SerializedName("FoundCategories")
                        val foundCategories: List<Listing>? = null)