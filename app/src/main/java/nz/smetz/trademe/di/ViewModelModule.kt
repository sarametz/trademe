package nz.smetz.trademe.di

import android.arch.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import nz.smetz.trademe.viewmodel.CategoryViewModel
import android.arch.lifecycle.ViewModelProvider
import nz.smetz.trademe.viewmodel.SearchFilterViewModel
import nz.smetz.trademe.viewmodel.SearchResultViewModel
import nz.smetz.trademe.viewmodel.TradeMeViewModelFactory

/**
 * Module for view model related bindings
 *
 * Created by smetz on 10/10/2017.
 */
@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(CategoryViewModel::class)
    abstract fun bindCategoryViewModel(categoryViewModel: CategoryViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SearchResultViewModel::class)
    abstract fun bindSearchResultViewModel(searchResultViewModel: SearchResultViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SearchFilterViewModel::class)
    abstract fun bindSearchFilterViewModel(searchFilterViewModel: SearchFilterViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: TradeMeViewModelFactory): ViewModelProvider.Factory
}