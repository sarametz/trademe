package nz.smetz.trademe.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import nz.smetz.trademe.MainActivity

/**
 * Module for enabling injection of the main activity
 *
 * Created by smetz on 10/9/2017.
 */
@Module
abstract class MainActivityModule {
    @ContributesAndroidInjector(modules = arrayOf(FragmentModule::class))
    abstract fun contributeMainActivity(): MainActivity
}