package nz.smetz.trademe.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import nz.smetz.trademe.view.fragment.CategoryFragment
import nz.smetz.trademe.view.fragment.SearchFilterFragment
import nz.smetz.trademe.view.fragment.SearchResultFragment

/**
 * Module that enables fragments to be injected
 *
 * Created by smetz on 10/10/2017.
 */
@Module
abstract class FragmentModule {
    @ContributesAndroidInjector
    abstract fun contributeCategoryFragment(): CategoryFragment

    @ContributesAndroidInjector
    abstract fun contributeSearchResultFragment(): SearchResultFragment

    @ContributesAndroidInjector
    abstract fun contributeSearchFilterFragment(): SearchFilterFragment
}