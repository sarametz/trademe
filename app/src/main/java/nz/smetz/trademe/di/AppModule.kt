package nz.smetz.trademe.di

import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import nz.smetz.trademe.model.TradeMeService
import nz.smetz.trademe.model.util.LiveDataCallAdapterFactory
import nz.smetz.trademe.model.util.OAuth1SigningInterceptor
import nz.smetz.trademe.model.util.TradeMeDateConverter
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.Date
import javax.inject.Singleton

/**
 * Module that provides application level services
 *
 * Created by smetz on 10/9/2017.
 */
@Module(includes = arrayOf(ViewModelModule::class))
class AppModule {
    @Singleton @Provides
    fun provideTradeMeService(): TradeMeService {
        val okHttpClient = OkHttpClient.Builder().addInterceptor(OAuth1SigningInterceptor()).build()
        val gson = GsonBuilder().registerTypeAdapter(Date::class.java,
                                                     TradeMeDateConverter()).create()
        return Retrofit.Builder()
                .baseUrl("https://api.tmsandbox.co.nz/v1/")
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(LiveDataCallAdapterFactory())
                .build()
                .create(TradeMeService::class.java)
    }
}