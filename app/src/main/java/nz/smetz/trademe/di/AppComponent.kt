package nz.smetz.trademe.di

import dagger.Component
import dagger.android.AndroidInjectionModule
import nz.smetz.trademe.TradeMeApp
import javax.inject.Singleton

/**
 * Dagger component for the application
 *
 * Created by smetz on 10/9/2017.
 */
@Singleton @Component(modules = arrayOf(AndroidInjectionModule::class,
                                        AppModule::class,
                                        MainActivityModule::class))
interface AppComponent {
    fun inject(tradeMeApp: TradeMeApp)
}