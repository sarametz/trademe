package nz.smetz.trademe.view

import android.support.v4.app.FragmentManager
import nz.smetz.trademe.MainActivity
import nz.smetz.trademe.R
import nz.smetz.trademe.model.data.SearchQuery
import nz.smetz.trademe.view.fragment.CategoryFragment
import nz.smetz.trademe.view.fragment.SearchFilterFragment
import nz.smetz.trademe.view.fragment.SearchResultFragment
import javax.inject.Inject

/**
 * Class that manages navigation between fragments
 *
 * Created by smetz on 10/11/2017.
 */
class NavigationController @Inject constructor(mainActivity: MainActivity) {
    private val fragmentManager: FragmentManager = mainActivity.supportFragmentManager

    fun navigateToCategory(number: String, addToBackStack: Boolean = true) {
        val fragmentTransaction = fragmentManager.beginTransaction()
                .replace(R.id.fragment_main, CategoryFragment.create(number))
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(null)
        }
        fragmentTransaction.commitAllowingStateLoss()
    }

    fun navigateToSearch(searchQuery: SearchQuery, addToBackStack: Boolean = true) {
        val fragmentTransaction = fragmentManager.beginTransaction()
                .replace(R.id.fragment_main, SearchResultFragment.create(searchQuery))
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(null)
        }
        fragmentTransaction.commitAllowingStateLoss()
    }

    fun showSearchFilterDialog(searchQuery: SearchQuery) {
        val fragment = SearchFilterFragment.create(searchQuery)
        fragment.show(fragmentManager, "filter")
    }

    fun showSearchFilterFragment(searchQuery: SearchQuery, addToBackStack: Boolean = true) {
        val fragmentTransaction = fragmentManager.beginTransaction()
                .replace(R.id.fragment_filter, SearchFilterFragment.create(searchQuery))
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(null)
        }
        fragmentTransaction.commitAllowingStateLoss()
    }
}