package nz.smetz.trademe.view

import android.view.View

/**
 * Interface for trade me activities with a filter header
 *
 * Created by smetz on 10/13/2017.
 */
interface TradeMeHeaderActivity {
    fun getFilterHeader(): View?
}