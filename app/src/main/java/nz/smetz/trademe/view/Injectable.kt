package nz.smetz.trademe.view

/**
 * Interface for fragments to determine if they should be injected
 *
 * Created by smetz on 10/10/2017.
 */
interface Injectable