package nz.smetz.trademe.view.fragment

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_category.category_recycler_view
import kotlinx.android.synthetic.main.fragment_category.swipe_refresh_layout
import nz.smetz.trademe.R
import nz.smetz.trademe.model.data.Category
import nz.smetz.trademe.model.util.ApiResponse
import nz.smetz.trademe.view.Injectable
import nz.smetz.trademe.view.NavigationController
import nz.smetz.trademe.view.adapter.CategoryListAdapter
import nz.smetz.trademe.viewmodel.CategoryViewModel
import nz.smetz.trademe.viewmodel.SearchFilterViewModel
import javax.inject.Inject

/**
 * Fragment to display a list of categories
 */
class CategoryFragment : Fragment(), Injectable {

    private var categoryViewModel: CategoryViewModel? = null
    private var searchFilterViewModel: SearchFilterViewModel? = null

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var navigationController: NavigationController

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val categoryNumber = arguments.getString(CATEGORY_NUMBER_KEY) ?: Category.ROOT_CATEGORY_NUMBER

        categoryViewModel = ViewModelProviders.of(this,
                                                  viewModelFactory).get(CategoryViewModel::class.java)
        searchFilterViewModel = ViewModelProviders.of(activity,
                                                      viewModelFactory).get(SearchFilterViewModel::class.java)

        categoryViewModel?.setCategoryNumber(categoryNumber)
        searchFilterViewModel?.setCategory(categoryNumber)

        categoryViewModel?.category?.observe(this,
         Observer<ApiResponse<Category>> { response ->
             swipe_refresh_layout.isRefreshing = false
             swipe_refresh_layout.isEnabled = false
             response?.data?.let {
                 (category_recycler_view.adapter as CategoryListAdapter).setCategory(context, it)
             }
         })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_category, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = CategoryListAdapter()
        adapter.categoryClickListener = object : CategoryListAdapter.CategoryClickListener {
            override fun onClick(category: Category) {
                categoryViewModel?.setCategoryNumber(category.number)
                searchFilterViewModel?.setCategory(category.number)
            }
        }
        category_recycler_view.adapter = adapter
        category_recycler_view.layoutManager = LinearLayoutManager(context)

        swipe_refresh_layout.setColorSchemeResources(R.color.colorAccent)
        swipe_refresh_layout.isEnabled = true
        swipe_refresh_layout.isRefreshing = true
        swipe_refresh_layout.setOnRefreshListener { categoryViewModel?.refresh() }
    }

    companion object {
        private val CATEGORY_NUMBER_KEY = "category_number"

        fun create(categoryNumber: String): CategoryFragment {
            val categoryFragment = CategoryFragment()
            val bundle = Bundle()
            bundle.putString(CATEGORY_NUMBER_KEY, categoryNumber)
            categoryFragment.arguments = bundle
            return categoryFragment
        }
    }
}
