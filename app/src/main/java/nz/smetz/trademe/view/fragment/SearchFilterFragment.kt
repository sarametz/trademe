package nz.smetz.trademe.view.fragment

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_search_filter.filter_done_button
import kotlinx.android.synthetic.main.fragment_search_filter.filter_view_pager
import nz.smetz.trademe.R
import nz.smetz.trademe.model.data.Category
import nz.smetz.trademe.model.data.SearchQuery
import nz.smetz.trademe.view.Injectable
import nz.smetz.trademe.view.NavigationController
import nz.smetz.trademe.view.adapter.SearchFilterPagerAdapter
import nz.smetz.trademe.viewmodel.SearchFilterViewModel
import javax.inject.Inject

/**
 * A dialog fragment for filtering a listing search
 */
class SearchFilterFragment : DialogFragment(), Injectable {

    private var searchFilterViewModel: SearchFilterViewModel? = null

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var navigationController: NavigationController

    init {
        setStyle(STYLE_NO_FRAME, R.style.FilterDialogTheme)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        // Use activity scope here so that the view model can be shared with child fragments
        searchFilterViewModel = ViewModelProviders.of(activity,
                                                  viewModelFactory).get(SearchFilterViewModel::class.java)

        searchFilterViewModel?.setQuery(arguments.getParcelable<SearchQuery>(SEARCH_QUERY_KEY))
        searchFilterViewModel?.searchQuery?.observe(this,
                                                    Observer<SearchQuery> { response ->
                                                        response?.let {
                                                            //update search results
                                                            navigationController.navigateToSearch(it)
                                                        }
                                                    })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_search_filter, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        filter_done_button?.setOnClickListener { dismiss() }
    }

    override fun onStart() {
        super.onStart()

        if (showsDialog) {
            dialog.window.setGravity(Gravity.TOP)
            dialog.window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                                    ViewGroup.LayoutParams.WRAP_CONTENT)
        }

        filter_view_pager?.adapter = SearchFilterPagerAdapter(childFragmentManager,
                                                             arguments.getParcelable<SearchQuery>(SEARCH_QUERY_KEY)?.category ?: Category.ROOT_CATEGORY_NUMBER)
    }

    companion object {
        private val SEARCH_QUERY_KEY = "search_query"

        fun create(searchQuery: SearchQuery): SearchFilterFragment {
            val searchFilterFragment = SearchFilterFragment()
            val bundle = Bundle()
            bundle.putParcelable(SEARCH_QUERY_KEY, searchQuery)
            searchFilterFragment.arguments = bundle
            return searchFilterFragment
        }
    }
}
