package nz.smetz.trademe.view.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.PagerAdapter
import nz.smetz.trademe.view.fragment.CategoryFragment
import kotlin.properties.Delegates

/**
 * Pager adapter for the search filters
 * //TODO add location and other filters
 *
 * Created by smetz on 10/13/2017.
 */
class SearchFilterPagerAdapter(fm: FragmentManager, private val categoryNumber: String): FragmentStatePagerAdapter(fm){

    override fun getItem(position: Int): Fragment {
        val fragment = CategoryFragment.create(categoryNumber)
        return fragment
    }

    override fun getCount(): Int = 1

    override fun getPageTitle(position: Int): CharSequence {
        //TODO get this string from a better place
        return "Category"
    }
}