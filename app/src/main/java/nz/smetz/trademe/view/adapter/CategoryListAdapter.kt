package nz.smetz.trademe.view.adapter

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.category_item.view.category_name
import nz.smetz.trademe.R
import nz.smetz.trademe.model.data.Category

/**
 * Adapter for displaying a list of categories
 *
 * Created by smetz on 10/10/2017.
 */
class CategoryListAdapter : RecyclerView.Adapter<CategoryListAdapter.CategoryViewHolder>() {
    private val items = mutableListOf<Category>()

    var categoryClickListener: CategoryClickListener? = null
    var selectedCategoryIsRoot: Boolean = false

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        holder.itemView.category_name.text = items[position].name

        when (holder.itemViewType) {
            VIEW_TYPE_SELECTED_CATEGORY ->
                holder.itemView.category_name.setTextColor(ContextCompat.getColor(holder.itemView.context, R.color.colorPrimaryDark))
            VIEW_TYPE_SUBCATEGORY ->
                holder.itemView.category_name.setPaddingRelative(
                        holder.itemView.resources.getDimensionPixelSize(R.dimen.subcategory_indent_padding_start),
                        0,
                        0,
                        0)
        }

        holder.itemView.setOnClickListener{ _ -> categoryClickListener?.onClick(items[position])}
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            position == 0 && !selectedCategoryIsRoot -> VIEW_TYPE_ROOT_CATEGORY
            position == 1 && !selectedCategoryIsRoot -> VIEW_TYPE_SELECTED_CATEGORY
            position == 0 && selectedCategoryIsRoot -> VIEW_TYPE_SELECTED_CATEGORY
            else -> VIEW_TYPE_SUBCATEGORY
        }
    }

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): CategoryViewHolder {
        val itemView = LayoutInflater.from(parent?.context)
                        .inflate(R.layout.category_item, parent, false)
        return CategoryViewHolder(itemView)
    }

    /**
     * Sets the selected category to display
     *
     * @param context the current context
     * @param category the category to display as current
     */
    fun setCategory(context: Context, category: Category) {
        val newItems = mutableListOf<Category>()
        // Add link to go back to all categories
        newItems.add(Category.root(context))

        // Add selected category
        if (category.isRoot()) {
            selectedCategoryIsRoot = true
        } else {
            selectedCategoryIsRoot = false
            newItems.add(category)
        }

        // Add subcategories of the selected item
        category.subcategories?.let {
            newItems.addAll(it)
        }
        update(newItems)
    }

    /**
     * Updates the categories to display in the list
     *
     * @param newItems the new categories to display
     */
    private fun update(newItems: List<Category>){
        items.clear()
        items.addAll(newItems)
        this.notifyDataSetChanged()
    }

    interface CategoryClickListener {
        fun onClick(category: Category)
    }

    class CategoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    companion object {
        val VIEW_TYPE_ROOT_CATEGORY = 0
        val VIEW_TYPE_SELECTED_CATEGORY = 1
        val VIEW_TYPE_SUBCATEGORY = 2
    }
}