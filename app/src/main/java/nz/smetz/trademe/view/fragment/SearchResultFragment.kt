package nz.smetz.trademe.view.fragment

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_search_result.empty_state_text_view
import kotlinx.android.synthetic.main.fragment_search_result.search_result_recycler_view
import kotlinx.android.synthetic.main.fragment_search_result.swipe_refresh_layout
import nz.smetz.trademe.R
import nz.smetz.trademe.model.data.Category
import nz.smetz.trademe.model.data.SearchQuery
import nz.smetz.trademe.model.data.SearchResult
import nz.smetz.trademe.model.util.ApiResponse
import nz.smetz.trademe.view.Injectable
import nz.smetz.trademe.view.NavigationController
import nz.smetz.trademe.view.TradeMeHeaderActivity
import nz.smetz.trademe.view.adapter.ListingListAdapter
import nz.smetz.trademe.viewmodel.SearchResultViewModel
import javax.inject.Inject
import kotlinx.android.synthetic.main.header_filter.view.category_path_text_view

/**
 * Fragment that shows a list of listings as a result of a search
 */
class SearchResultFragment : Fragment(), Injectable {

    private var searchResultViewModel: SearchResultViewModel? = null

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var navigationController: NavigationController

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        searchResultViewModel = ViewModelProviders.of(this,
                                                      viewModelFactory).get(SearchResultViewModel::class.java)

        searchResultViewModel?.setSearchQuery(arguments.getParcelable<SearchQuery>(SEARCH_QUERY_KEY))
        searchResultViewModel?.searchResult?.observe(this,
        Observer<ApiResponse<SearchResult>> { response ->
            swipe_refresh_layout.isRefreshing = false
            (search_result_recycler_view.adapter as ListingListAdapter).update(
                response?.data?.list ?: emptyList())
            if (response?.data?.list?.isEmpty() ?: true) {
                search_result_recycler_view.visibility = View.GONE
                empty_state_text_view.visibility = View.VISIBLE
            } else {
                search_result_recycler_view.visibility = View.VISIBLE
                empty_state_text_view.visibility = View.GONE
            }
        })

        (activity as? TradeMeHeaderActivity)?.let {
            searchResultViewModel?.searchCategory?.observe(this,
               Observer<ApiResponse<Category>> { response ->
                   it.getFilterHeader()?.category_path_text_view?.text = response?.data?.displayPath
               })
            it.getFilterHeader()?.setOnClickListener {
                navigationController.showSearchFilterDialog(arguments.getParcelable<SearchQuery>(SEARCH_QUERY_KEY))
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_search_result, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        search_result_recycler_view.adapter = ListingListAdapter()
        search_result_recycler_view.layoutManager = LinearLayoutManager(context)

        swipe_refresh_layout.setColorSchemeResources(R.color.colorAccent)
        swipe_refresh_layout.isRefreshing = true
        swipe_refresh_layout.setOnRefreshListener { searchResultViewModel?.refresh() }
    }

    companion object {
        private val SEARCH_QUERY_KEY = "search_query"

        fun create(searchQuery: SearchQuery): SearchResultFragment {
            val searchResultFragment = SearchResultFragment()
            val bundle = Bundle()
            bundle.putParcelable(SEARCH_QUERY_KEY, searchQuery)
            searchResultFragment.arguments = bundle
            return searchResultFragment
        }
    }
}
