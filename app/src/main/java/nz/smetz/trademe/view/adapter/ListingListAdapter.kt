package nz.smetz.trademe.view.adapter

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.listing_item.view.listing_buy_now_container
import kotlinx.android.synthetic.main.listing_item.view.listing_buy_now_price
import kotlinx.android.synthetic.main.listing_item.view.listing_buy_now_status
import kotlinx.android.synthetic.main.listing_item.view.listing_container
import kotlinx.android.synthetic.main.listing_item.view.listing_image_thumbnail
import kotlinx.android.synthetic.main.listing_item.view.listing_price
import kotlinx.android.synthetic.main.listing_item.view.listing_region
import kotlinx.android.synthetic.main.listing_item.view.listing_reserve_status
import kotlinx.android.synthetic.main.listing_item.view.listing_title
import nz.smetz.trademe.R
import nz.smetz.trademe.model.data.Listing

/**
 * Adapter for displaying a list of listings
 *
 * Created by smetz on 10/10/2017.
 */
class ListingListAdapter : RecyclerView.Adapter<ListingListAdapter.ListingViewHolder>() {
    private val items = mutableListOf<Listing>()

    var listingClickListener: ListingClickListener? = null

    override fun onBindViewHolder(holder: ListingViewHolder?, position: Int) {
        holder?.itemView?.let { itemView ->
            val item = items[position]
            itemView.listing_region?.text = item.region
            itemView.listing_title?.text = item.title
            itemView.listing_price?.text = item.priceDisplay

            when (item.reserveState) {
                Listing.ReserveState.None.intValue -> {
                    itemView.listing_reserve_status?.text =
                        itemView.context.resources.getString(R.string.reserve_state_no_reserve)
                }
                Listing.ReserveState.Met.intValue -> {
                    itemView.listing_reserve_status?.text =
                        itemView.context.resources.getString(R.string.reserve_state_reserve_met)
                }
                Listing.ReserveState.NotMet.intValue -> {
                    itemView.listing_reserve_status?.text =
                        itemView.context.resources.getString(R.string.reserve_state_reserve_not_met)
                }
                else -> {
                    if (item.isBuyNowOnly) itemView.listing_price?.visibility = View.GONE
                    itemView.listing_reserve_status?.visibility = View.GONE
                }
            }

            if (item.hasBuyNow) {
                itemView.listing_buy_now_price?.text = itemView.context.resources
                        .getString(R.string.buy_now_price_format)
                        .format(item.buyNowPrice)
                itemView.listing_buy_now_status?.visibility = View.VISIBLE
            } else {
                itemView.listing_buy_now_container?.visibility = View.GONE
            }

            val bgColor = if (item.isHighlighted) R.color.highlightColor else R.color.white
            itemView.listing_container?.setBackgroundColor(ContextCompat.getColor(holder.itemView.context, bgColor))

            Glide.with(itemView.context)
                    .load(item.pictureHref)
                    .placeholder(R.drawable.listing_image_placeholder)
                    .into(itemView.listing_image_thumbnail)

            itemView.setOnClickListener { _ -> listingClickListener?.onClick(item) }
        }
    }

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ListingViewHolder {
        val itemView = LayoutInflater.from(parent?.context)
                .inflate(R.layout.listing_item, parent, false)
        return ListingViewHolder(itemView)
    }

    override fun onViewRecycled(holder: ListingViewHolder?) {
        super.onViewRecycled(holder)
        Glide.clear(holder?.itemView?.listing_image_thumbnail)
    }

    /**
     * Updates the listings to display in the list
     *
     * @param newItems the new listings to display
     */
    fun update(newItems: List<Listing>){
        items.clear()
        items.addAll(newItems)
        this.notifyDataSetChanged()
    }

    interface ListingClickListener {
        fun onClick(listing: Listing)
    }

    class ListingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}
