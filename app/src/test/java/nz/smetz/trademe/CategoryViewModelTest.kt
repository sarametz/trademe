package nz.smetz.trademe

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import nz.smetz.trademe.model.TradeMeService
import nz.smetz.trademe.model.data.Category
import nz.smetz.trademe.model.util.ApiResponse
import nz.smetz.trademe.viewmodel.CategoryViewModel
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.Response

/**
 * Unit tests for the category view model
 * TODO: Add more tests here
 *
 * Created by smetz on 10/11/2017.
 */
@RunWith(JUnit4::class)
class CategoryViewModelTest {
    @Rule
    @JvmField
    var instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var categoryViewModel: CategoryViewModel
    private lateinit var mockService: TradeMeService

    @Before
    fun setup() {
        mockService = mock<TradeMeService>()
        categoryViewModel = CategoryViewModel(mockService)
    }

    /**
     * Tests retrieving a category from the category view model
     */
    @Test
    fun getCategory() {
        val categoryLiveData = MutableLiveData<ApiResponse<Category>>()
        whenever(mockService.getCategory("0153-")).then { _ ->
            categoryLiveData.postValue(fakeClothingApiResponse)
            return@then categoryLiveData
        }

        categoryViewModel.setCategoryNumber("0153-")
        val observer = mock<Observer<ApiResponse<Category>>>()
        categoryViewModel.category.observeForever(observer)

        verify(observer).onChanged(fakeClothingApiResponse)
    }

    /**
     * Tests that refreshing the category re-sends the network request
     */
    @Test
    fun refreshCategory() {
        val categoryLiveData = MutableLiveData<ApiResponse<Category>>()
        whenever(mockService.getCategory("0153-")).then { _ ->
            categoryLiveData.postValue(fakeClothingApiResponse)
            return@then categoryLiveData
        }

        categoryViewModel.setCategoryNumber("0153-")
        val observer = mock<Observer<ApiResponse<Category>>>()
        categoryViewModel.category.observeForever(observer)

        verify(observer).onChanged(fakeClothingApiResponse)

        whenever(mockService.getCategory("0153-")).then { _ ->
            categoryLiveData.postValue(fakeUpdatedClothingApiResponse)
            return@then categoryLiveData
        }
        categoryViewModel.refresh()

        verify(observer).onChanged(fakeUpdatedClothingApiResponse)
    }

    /**
     * Tests that changing the category number updates the category live data
     */
    @Test
    fun updateCategory() {
        val categoryLiveData = MutableLiveData<ApiResponse<Category>>()
        whenever(mockService.getCategory("0153-")).then { _ ->
            categoryLiveData.postValue(fakeClothingApiResponse)
            return@then categoryLiveData
        }

        categoryViewModel.setCategoryNumber("0153-")
        val observer = mock<Observer<ApiResponse<Category>>>()
        categoryViewModel.category.observeForever(observer)

        verify(observer).onChanged(fakeClothingApiResponse)

        whenever(mockService.getCategory("0153-updated")).then { _ ->
            categoryLiveData.postValue(fakeUpdatedClothingApiResponse)
            return@then categoryLiveData
        }

        categoryViewModel.setCategoryNumber("0153-updated")
        verify(observer).onChanged(fakeUpdatedClothingApiResponse)
    }

    companion object {
        val fakeClothingApiResponse = ApiResponse(Response.success(Category("Clothing & Fashion",
                                                                            "0153-",
                                                                            "/Clothing-Fashion")))

        val fakeUpdatedClothingApiResponse = ApiResponse(Response.success(Category("Updated Clothing & Fashion",
                                                                                   "0153-updated",
                                                                                   "/Updated-Clothing-Fashion")))
    }
}