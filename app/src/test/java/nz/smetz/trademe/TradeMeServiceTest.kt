package nz.smetz.trademe

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.gson.GsonBuilder
import nz.smetz.trademe.model.TradeMeService
import nz.smetz.trademe.model.data.Category
import nz.smetz.trademe.model.data.Listing
import nz.smetz.trademe.model.data.SearchQuery
import nz.smetz.trademe.model.data.SearchResult
import nz.smetz.trademe.model.util.LiveDataCallAdapterFactory
import nz.smetz.trademe.model.util.TradeMeDateConverter
import nz.smetz.trademe.util.getTestValue
import nz.smetz.trademe.util.enqueueTradeMeResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

/**
 * Tests for the TradeMe API service
 * TODO: Add more tests here
 *
 * Created by smetz on 10/9/2017.
 */
@RunWith(JUnit4::class)
class TradeMeServiceTest {
    @Rule
    @JvmField
    var instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var service: TradeMeService
    private lateinit var mockWebServer: MockWebServer

    @Before
    fun createService() {
        mockWebServer = MockWebServer()
        val gson = GsonBuilder().registerTypeAdapter(Date::class.java,
                                                     TradeMeDateConverter()).create()
        service = Retrofit.Builder()
                .baseUrl(mockWebServer.url("/"))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(LiveDataCallAdapterFactory())
                .build()
                .create(TradeMeService::class.java)
    }

    @After
    fun stopService() {
        mockWebServer.shutdown()
    }

    @Test
    fun getAllCategories() {
        mockWebServer.enqueueTradeMeResponse("all_categories.json")

        val rootCategory = service.getAllCategories().getTestValue().data

        assertNotNull(rootCategory)
        assertEquals("Root", rootCategory?.name)
        assertEquals("", rootCategory?.number)
        assertEquals("", rootCategory?.path)
        assertEquals(27, rootCategory?.subcategories?.size)
    }

    @Test
    fun getCategory() {
        mockWebServer.enqueueTradeMeResponse("0153-.json")

        val clothingCategory = service.getCategory("0153-").getTestValue().data

        val expectedClothingCategory = Category("Clothing & Fashion",
                                                "0153-",
                                                "/Clothing-Fashion",
                                                listOf(Category("Boys",
                                                                "0153-0435-",
                                                                "/Clothing-Fashion/Boys",
                                                                canHaveSecondCategory = true),
                                                       Category("Girls",
                                                                "0153-0436-",
                                                                "/Clothing-Fashion/Girls",
                                                                canHaveSecondCategory = true),
                                                       Category("Men",
                                                                "0153-0438-",
                                                                "/Clothing-Fashion/Men",
                                                                canHaveSecondCategory = true),
                                                       Category("Women",
                                                                "0153-0439-",
                                                                "/Clothing-Fashion/Women",
                                                                canHaveSecondCategory = true)),
                                                canHaveSecondCategory = true)

        assertEquals(expectedClothingCategory, clothingCategory)
    }

    @Test
    fun searchListings() {
        mockWebServer.enqueueTradeMeResponse("search_jackets_one_item_response.json")

        val searchResult = service.searchListings(SearchQuery(category = "3720").toMap()).getTestValue().data

        val expectedListing = Listing(6055780,
                                      "Mens Fashionista Overcoat - L",
                                      "0153-0438-3720-",
                                      117.0,
                                      137.0,
                                      Date(1507595660657),
                                      Date(1508200380000),
                                      true,
                                      true,
                                      true,
                                      false,
                                      false,
                                      0.0,
                                      "/Clothing-Fashion/Men/Jackets",
                                      "https://images.tmsandbox.co.nz/photoserver/thumb/893921.jpg",
                                      "$117.00",
                                      "Auckland",
                                      false,
                                      true,
                                      true,
                                      false,
                                      false,
                                      2)
        val expectedSearchResult = SearchResult(36, 1, 1, listOf(expectedListing), "", emptyList())

        assertEquals(expectedSearchResult, searchResult)
    }
}