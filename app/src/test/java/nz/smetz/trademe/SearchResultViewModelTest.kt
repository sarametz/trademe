package nz.smetz.trademe

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import nz.smetz.trademe.model.TradeMeService
import nz.smetz.trademe.model.data.Listing
import nz.smetz.trademe.model.data.SearchQuery
import nz.smetz.trademe.model.data.SearchResult
import nz.smetz.trademe.model.util.ApiResponse
import nz.smetz.trademe.viewmodel.SearchResultViewModel
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.Response
import java.util.*

/**
 * Unit tests for the category view model
 * TODO: Add more tests here
 *
 * Created by smetz on 10/11/2017.
 */
@RunWith(JUnit4::class)
class SearchResultViewModelTest {
    @Rule
    @JvmField
    var instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var searchResultViewModel: SearchResultViewModel
    private lateinit var mockService: TradeMeService

    @Before
    fun setup() {
        mockService = mock<TradeMeService>()
        searchResultViewModel = SearchResultViewModel(mockService)
    }

    /**
     * Tests searching from the search result view model
     */
    @Test
    fun searchCategory() {
        val searchResultLiveData = MutableLiveData<ApiResponse<SearchResult>>()
        whenever(mockService.searchListings(SearchQuery(category = "0153-0438-3720-").toMap())).then { _ ->
            searchResultLiveData.postValue(oneSearchResultApiResponse)
            return@then searchResultLiveData
        }

        searchResultViewModel.setSearchQuery(SearchQuery(category = "0153-0438-3720-"))
        val observer = mock<Observer<ApiResponse<SearchResult>>>()
        searchResultViewModel.searchResult.observeForever(observer)

        verify(observer).onChanged(oneSearchResultApiResponse)
    }

    /**
     * Tests that refreshing the search result re-sends the network request
     */
    @Test
    fun refreshSearchResult() {
        val searchResultLiveData = MutableLiveData<ApiResponse<SearchResult>>()
        whenever(mockService.searchListings(SearchQuery(category = "0153-0438-3720-").toMap())).then { _ ->
            searchResultLiveData.postValue(oneSearchResultApiResponse)
            return@then searchResultLiveData
        }

        searchResultViewModel.setSearchQuery(SearchQuery(category = "0153-0438-3720-"))
        val observer = mock<Observer<ApiResponse<SearchResult>>>()
        searchResultViewModel.searchResult.observeForever(observer)

        verify(observer).onChanged(oneSearchResultApiResponse)

        whenever(mockService.searchListings(SearchQuery(category = "0153-0438-3720-").toMap())).then { _ ->
            searchResultLiveData.postValue(twoSearchResultsApiResponse)
            return@then searchResultLiveData
        }
        searchResultViewModel.refresh()

        verify(observer).onChanged(twoSearchResultsApiResponse)
    }

    /**
     * Tests that changing the category number updates the search result data
     */
    @Test
    fun updateSearchCategory() {
        val searchResultLiveData = MutableLiveData<ApiResponse<SearchResult>>()
        whenever(mockService.searchListings(SearchQuery(category = "0153-0438-3720-").toMap())).then { _ ->
            searchResultLiveData.postValue(oneSearchResultApiResponse)
            return@then searchResultLiveData
        }

        searchResultViewModel.setSearchQuery(SearchQuery(category = "0153-0438-3720-"))
        val observer = mock<Observer<ApiResponse<SearchResult>>>()
        searchResultViewModel.searchResult.observeForever(observer)

        verify(observer).onChanged(oneSearchResultApiResponse)

        whenever(mockService.searchListings(SearchQuery(category = "0153-0438-3720-updated").toMap())).then { _ ->
            searchResultLiveData.postValue(twoSearchResultsApiResponse)
            return@then searchResultLiveData
        }

        searchResultViewModel.setSearchQuery(SearchQuery(category = "0153-0438-3720-updated"))
        verify(observer).onChanged(twoSearchResultsApiResponse)
    }

    companion object {
        val expectedFirstListing = Listing(6055780,
                                           "Mens Fashionista Overcoat - L",
                                           "0153-0438-3720-",
                                           117.0,
                                           137.0,
                                           Date(1507595660657),
                                           Date(1508200380000),
                                           true,
                                           true,
                                           true,
                                           false,
                                           false,
                                           0.0,
                                           "/Clothing-Fashion/Men/Jackets",
                                           "https://images.tmsandbox.co.nz/photoserver/thumb/893921.jpg",
                                           "$117.00")
        val expectedSecondListing = Listing(6064601,
                                            "Pro1 Delta 0862",
                                            "0153-0438-3720-",
                                            12.22,
                                            47.5,
                                            Date(1507837813690),
                                            Date(1508010540000),
                                            false,
                                            true,
                                            false,
                                            false,
                                            false,
                                            0.0,
                                            "/Clothing-Fashion/Men/Jackets",
                                            "https://images.tmsandbox.co.nz/photoserver/thumb/3145436.jpg",
                                            "$12.22")

        val oneSearchResultApiResponse = ApiResponse(Response.success(SearchResult(36,
                                                                                   1,
                                                                                   1,
                                                                                   listOf(expectedFirstListing),
                                                                                   "",
                                                                                   emptyList())))
        val twoSearchResultsApiResponse = ApiResponse(Response.success(SearchResult(36,
                                                                                    1,
                                                                                    1,
                                                                                    listOf(expectedFirstListing,
                                                                                           expectedSecondListing),
                                                                                    "",
                                                                                    emptyList())))
    }
}