package nz.smetz.trademe.util

import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import java.nio.charset.StandardCharsets

/**
 * Extensions for MockWebServer used for unit testing
 *
 * Created by smetz on 10/12/2017.
 */
fun MockWebServer.enqueueTradeMeResponse(fileName: String) {
    val responseBody = javaClass.classLoader.getResource(fileName).readText(StandardCharsets.UTF_8)
    val mockResponse = MockResponse().setBody(responseBody)
    this.enqueue(mockResponse)
}