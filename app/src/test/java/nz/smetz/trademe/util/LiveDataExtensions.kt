package nz.smetz.trademe.util

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

/**
 * Utility method to get live data value in unit tests
 * Based on the LiveDataTestUtil in https://github.com/googlesamples/android-architecture-components/tree/master/GithubBrowserSample
 *
 * Created by smetz on 10/10/2017.
 */
@Suppress("UNCHECKED_CAST")
fun <T> LiveData<T>.getTestValue(): T {
    val data = arrayOfNulls<Any>(1)
    val latch = CountDownLatch(1)
    val observer = object : Observer<T> {
        override fun onChanged(o: T?) {
            data[0] = o
            latch.countDown()
            this@getTestValue.removeObserver(this)
        }
    }
    this.observeForever(observer)
    latch.await(2, TimeUnit.SECONDS)

    return data[0] as T
}