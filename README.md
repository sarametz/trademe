# Trade Me Lite #

A simplified Trade Me app that allows the user to browse the Trade Me categories and displays the first page of listings in each category

#### Libraries used to make this:

 * [Kotlin](https://kotlinlang.org/docs/reference/)
 * [Retrofit](http://square.github.io/retrofit/)
 * [Android Architecure Components](https://developer.android.com/topic/libraries/architecture/index.html)
 * [Dagger](https://google.github.io/dagger/)
 * [Glide](https://github.com/bumptech/glide) 
 

Contact Sara Metz (smetz@hotmail.co.nz) for more information

![Phone Portrait](images/Screenshot_1508093784.png)
![Phone Filter Dialog](images/Screenshot_1508093488.png)
![Phone Landscape](images/Screenshot_1508093550.png)

![Tablet Portrait](images/Screenshot_1508093625.png)
![Tablet Landscape](images/Screenshot_1508093696.png)